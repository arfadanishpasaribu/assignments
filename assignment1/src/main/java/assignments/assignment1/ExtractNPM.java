package assignments.assignment1;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean
        boolean akhir = true;

        // npm length
        int npm_length = Long.toString(npm).length();

        if(npm_length != 14){
            akhir = false;
        }else{
            // npm to array
            String temp = Long.toString(npm);
            int[] numbers = new int[temp.length()];
            for (int i = 0; i < temp.length(); i++) {
                numbers[i] = temp.charAt(i) - '0';
            }

            // cheking jurusan
            String jurusan1 = Integer.toString(numbers[2]);
            String jurusan2 = Integer.toString(numbers[3]);
            String jurusan = jurusan1 + jurusan2;
            String[] daftar_jurusan = {"01","02","03","11","12"};
            List<String> nameList = new ArrayList<>(Arrays.asList(daftar_jurusan));
            akhir = nameList.contains(jurusan);

            // checking tanggal lahir
            String day_str = Integer.toString(numbers[4]) + Integer.toString(numbers[5]);
            int day = Integer.parseInt(day_str);
            if(day > 31){
                akhir = false;
            }
            else{
                String month_str = Integer.toString(numbers[6]) + Integer.toString(numbers[7]);
                int month = Integer.parseInt(month_str);
                if(month > 12){
                    akhir = false;
                } else {
                    String year_str = Integer.toString(numbers[8]) + Integer.toString(numbers[9]) + Integer.toString(numbers[10])+ Integer.toString(numbers[11]);
                    int year = Integer.parseInt(year_str);
                    if(2021-year < 15){
                        akhir = false;
                    } else {
                        int sum = 0;
                        int batas = numbers.length-2;
                        for(int i = 0;i < 6;i++){
                            int temp_sum = numbers[i] * numbers[batas];
                            sum += temp_sum;
                            batas -= 1;
                        }



                        while(sum >= 10){
                            String temp_sum_str = Long.toString(sum);
                            int[] numbers_sum = new int[temp_sum_str.length()];
                            for (int i = 0; i < temp_sum_str.length(); i++) {
                                numbers_sum[i] = temp_sum_str.charAt(i) - '0';
                            }
                            sum = 0;
                            batas = numbers_sum.length-1;
                            for(int i = 0;i < numbers_sum.length/2;i++){
                                int temp_sum2 = numbers_sum[i] + numbers_sum[batas];
                                sum += temp_sum2;
                                batas -= 1;
                            }
                            if (sum >= 10){
                                sum = 0;
                                batas = numbers_sum.length-1;
                                for(int i = 0;i < numbers_sum.length/2;i++){
                                    int temp_sum2 = numbers_sum[i] * numbers_sum[batas];
                                    sum += temp_sum2;
                                    batas -= 1;
                                }
                            }
                        }
                        if (sum != numbers[13]){
                            akhir = false;
                        }
                    }

                }
            }
        }
        return akhir;

    }

    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format

        String temp = Long.toString(npm);
        int[] numbers = new int[temp.length()];
        for (int i = 0; i < temp.length(); i++) {
            numbers[i] = temp.charAt(i) - '0';
        }
        String masuk = Integer.toString(numbers[0]) + Integer.toString(numbers[1]);
        String day_str = Integer.toString(numbers[4]) + Integer.toString(numbers[5]);
        String month_str = Integer.toString(numbers[6]) + Integer.toString(numbers[7]);
        String year_str = Integer.toString(numbers[8]) + Integer.toString(numbers[9]) + Integer.toString(numbers[10])+ Integer.toString(numbers[11]);

        String jurusan1 = Integer.toString(numbers[2]);
        String jurusan2 = Integer.toString(numbers[3]);
        String jurusan = jurusan1 + jurusan2;

        System.out.println("Tahun masuk: 20"+masuk);
        switch(jurusan){
            case "01":
                System.out.println("Jurusan: Ilmu Komputer");
                break;
            case "02":
                System.out.println("Jurusan: Sistem Informasi");
                break;
            case "03":
                System.out.println("Jurusan: Teknologi Informasi");
                break;
            case "11":
                System.out.println("Jurusan: Teknik Telekomunikasi");
                break;
            case "12":
                System.out.println("Jurusan: Teknik Elektro");
        }

        System.out.println("Tanggal Lahir: "+day_str+"-"+month_str+"-"+year_str);

        return "";

    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            else{
                if(validate(npm) == true){
                    extract(npm);
                } else {
                    System.out.println("NPM tidak valid!");
                }
            }

            
        }
        input.close();
    }
}